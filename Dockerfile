FROM golang:1.15.8-alpine3.13

RUN apk update && apk add \
	git\
	openssh\
	&& rm -rf /var/cache/apk/*

RUN apk --no-cache add --virtual .build-deps --update \
        curl \
        git \
        gcc \
        make \
        openssh

RUN git clone https://github.com/prometheus-community/PushProx.git && \
    cd PushProx && \
    make build

USER nobody
ENTRYPOINT ["/go/PushProx/pushprox-proxy"]
